var showLogs = true;
if (showLogs) {
    console.log("Inside ScreenManager");
}
var gameWidth = 800;
var gameHeight = 450;

var backgroundColor = "#B0171F";

var game = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, 'KulturGameDev', { preload: preload, create: create, update: update });
var keys;

var PlayerCharacter;

function preload () {

    if (showLogs) {

        console.log("ScreenManager - preload");
    }
    game.load.image('background'    ,   'Pictures/bg.png');
    game.load.atlas('mario_walking' ,   'Sprites/MarioSprite_.png', 'Sprites/Mariosprite_.json', 
    Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);

}

function create() {

    // game.physics.startSystem(Phaser.Physics.ARCADE);

    if (showLogs)

    {
        console.log("ScreenManager -  create");
}
   
   

    var Background = game.add.sprite(game.world.centerX, game.world.centerY, 'background');
    Background.anchor.setTo(0.5, 0.5);


    keys = game.input.keyboard.createCursorKeys();

    PlayerCharacter = new GameObjects.Character();

    PlayerCharacter.init("mario");

   game.physics.arcade.enable(PlayerCharacter);

   //PlayerCharacter.body.bounce.y = 0.2;
    //PlayerCharacter.body.gravity.y = 300;
   
}


function update() {

    if (keys.left.isDown) 

    {
        PlayerCharacter.MoveLeft();
    }
    else if (keys.right.isDown) 
    {

        PlayerCharacter.MoveRight();
    }
    else if (keys.up.isDown)//&& PlayerCharacter.body.touching.down)
     {

      // PlayerCharacter.body.velocity.y = -350;
        PlayerCharacter.Jump();
    }
    else {

        PlayerCharacter.Stand();
    }

}


