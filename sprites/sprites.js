﻿{
"frames": {
	"m1.png": {
		"frame": {"x":130, "y":0, "w":57, "h":95},
		"spriteSourceSize": {"x":0,"y":0,"w":57,"h":95},
		"sourceSize": {"w":57,"h":95}
	},
	"m2.png": {
		"frame": {"x":130, "y":96, "w":51, "h":98},
		"spriteSourceSize": {"x":0,"y":0,"w":51,"h":98},
		"sourceSize": {"w":51,"h":98}
	},
	"m3.png": {
		"frame": {"x":0, "y":0, "w":68, "h":93},
		"spriteSourceSize": {"x":0,"y":0,"w":68,"h":93},
		"sourceSize": {"w":68,"h":93}
	},
	"m4.png": {
		"frame": {"x":0, "y":94, "w":68, "h":94},
		"spriteSourceSize": {"x":0,"y":0,"w":68,"h":94},
		"sourceSize": {"w":68,"h":94}
	},
	"m5.png": {
		"frame": {"x":182, "y":96, "w":51, "h":95},
		"spriteSourceSize": {"x":0,"y":0,"w":51,"h":95},
		"sourceSize": {"w":51,"h":95}
	},
	"m6.png": {
		"frame": {"x":69, "y":100, "w":60, "h":97},
		"spriteSourceSize": {"x":0,"y":0,"w":60,"h":97},
		"sourceSize": {"w":60,"h":97}
	},
	"m7.png": {
		"frame": {"x":0, "y":189, "w":61, "h":89},
		"spriteSourceSize": {"x":0,"y":0,"w":61,"h":89},
		"sourceSize": {"w":61,"h":89}
	},
	"m8.png": {
		"frame": {"x":69, "y":0, "w":60, "h":99},
		"spriteSourceSize": {"x":0,"y":0,"w":60,"h":99},
		"sourceSize": {"w":60,"h":99}
	}

},
"meta": {
	"image": "sprites.png",
	"size": {"w": 234, "h": 279},
	"scale": "1"
}
}